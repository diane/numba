Source: numba
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all-dev,
 python3-colorama (>= 0.3.9~),
 python3-llvmlite (>= 0.35~),
 python3-numpy (>= 1.10~),
 python3-pip,
 sphinx, dh-sequence-sphinxdoc,
 python3-scipy (>= 1.0),
 python3-sphinx-rtd-theme,
 python3-pytest,
 python3-setuptools,
 python3-numpydoc,
 python3-pytest-xdist,
 libtbb-dev,
Standards-Version: 4.5.1
Homepage: http://numba.pydata.org/
Vcs-Git: https://salsa.debian.org/science-team//numba.git
Vcs-Browser: https://salsa.debian.org/science-team/numba

Package: python3-numba
Architecture: any
Section: python
Depends:
 python3-llvmlite (>= 0.27.0~),
 python3-scipy (>= 1.0),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Recommends:
 numba-doc,
 python3-pip
Suggests:
 nvidia-cuda-toolkit [amd64 ppc64el]
Description: native machine code compiler for Python 3
 Numba compiles native machine code instructions from Python programs at
 runtime using the LLVM compiler infrastructure. It could be easily employed
 by decorating individual computation intensive functions in the Python code.
 Numba could significantly speed up the performance of computations, and
 optionally supports compilation to run on GPU processors through Nvidia's
 CUDA platform.
 It integrates well with the Python scientific software stack, and
 especially recognizes Numpy arrays.
 .
 This package contains the modules for Python 3.

Package: numba-doc
Architecture: all
Section: doc
Depends:
 ${sphinxdoc:Depends},
 ${misc:Depends}
Recommends: python3-numba
Description: native machine code compiler for Python (docs)
 Numba compiles native machine code instructions from Python programs at
 runtime using the LLVM compiler infrastructure. Just-in-time compilation with
 Numba could be easily employed by decorating individual computation intensive
 functions in the Python code.
 Numba could significantly speed up the performance of computations, and
 optionally supports compilation to run on GPU processors through Nvidia's
 CUDA platform.
 It integrates well with the Python scientific software stack, and
 especially recognizes Numpy arrays.
 .
 This package contains the documentation and examples.
